# bgcolor

Detect background color using the ANSI code `\e]11;?\a`

## Usage

    $ gcc -Wall -o bgcolor bgcolor.c
    $ ./bgcolor

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <termios.h>

#define CODE "\033]11;?\007"

int main() {

  char buf[25];
  int data;
  struct termios tp, save;

  if (tcgetattr(STDIN_FILENO, &tp) == -1) {
    perror("tcgetattr");
    return 1;
  }

  // Disable buffering and echo

  save = tp;
  tp.c_lflag &= (~ICANON & ~ECHO);

  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &tp) == -1) {
    perror("tcsetattr");
    return 1;
  }

  write(1, CODE, strlen(CODE));

  data = read(0, buf, sizeof(buf));
  buf[data] = 0;

  tcsetattr(STDIN_FILENO, TCSANOW, &save);

  printf("Current background: %s\n", buf + 9);

  return 0;

}
